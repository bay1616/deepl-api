import requests
from flask import Flask, jsonify, request

def read_file(path):
    with open(path, 'r') as file:
        data = file.read().replace("\n", "")
    return data

def get_translation(text, target_lang):
    args = {"auth_key": auth_key, "text": text, "target_lang": target_lang, "source_lang": "DE"}
    r = requests.get("https://api.deepl.com/v2/translate", params = args)
    return r.text

auth_key = read_file("auth_key")
app = Flask(__name__)

@app.route("/translate", methods=["POST"])
def get_abstracts_for_query_srf_api():
    text = request.form.get("text")
    target_lang = request.form.get("target_lang")
    return get_translation(text, target_lang)

app.config['JSON_AS_ASCII'] = False
app.run(host="0.0.0.0", port=int("5050"), debug=True)