FROM python:3.8-buster

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY auth_key auth_key
COPY deepl_api.py deepl_api.py

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["deepl_api.py"]
