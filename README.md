# DeepL API
This service receives a string to translate and returns the translation in the desired language.

This service is part of the [Related Items project](https://gitlab.rrz.uni-hamburg.de/bay1620/wilps-related-items/).

## Local Setup from repository
Due to the secret AUTH KEY there is no possibility here to use an image from Docker Hub. You have to follow these instructions instead:
- Create a file named 'auth_key' in this folder
- Put your AUTH KEY from DeepL Pro into the file (nothing else)
- Build the Docker image from this folder: `docker build -t deepl-api .`
- Run the Docker container: `docker run -p 6061:5050 deepl-api`

## Usage
- `curl -F text="Guten Abend" -F target_lang=EN -X POST http://0.0.0.0:6061/translate`
